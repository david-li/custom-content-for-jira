Custom Content plugin for JIRA
==============================

What is it
----------

This plugin provides a gadget for displaying custom content like web sites and images for JIRA.

More information can be found on 
[Atlassian Marketplace - Custom Content plugin](https://marketplace.atlassian.com/manage/plugins/com.tngtech.jira.plugins.customcontent.jira-gadget-custom-content).
